import { Injectable } from '@angular/core';
import { Subject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  public isLoading = new Subject<boolean>();
  public isLoad = new Subject<boolean>();
  public progressSpinner = new Subject<boolean>();

  /**
   * It loads the spinner for every ajax call
   */
  show() {
    this.isLoading.next(true);
  }

  /**
   * Hides loader spinner after completing ajax call
   */
  hide() {
    this.isLoading.next(false);
  }
}
