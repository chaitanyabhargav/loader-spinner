import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoaderService } from './../services/loader.service';
import { finalize } from "rxjs/operators";


@Injectable()
export class SpinnerInterceptor implements HttpInterceptor {

  constructor(private loaderService: LoaderService) {}

  /**
   * Interceptor clones the outgoing request and adds extra information to the requests.
   * @param request HttpRequest
   * @param next HttpHandler
   * @returns intercept
   */
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    this.loaderService.show()
    return next.handle(request).pipe(finalize(() => this.loaderService.hide()));
  }
}
